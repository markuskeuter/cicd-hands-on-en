# Lesson 2 Provisioning
You'll use Google Cloud Compute Engine (GCE) and more specific the Google Cloud Shell in this tutorial. This tutroial will demonstrate how virtual machines can be provisoned, how to apply the basics for immutable infrastructure or infrastructure as code principles. You'll need at least the following access rights to the project within the Google Cloud environment:


Voor provisioning maken we gebruik van Google Cloud Compute Egine (GCE) en specifiek werken we met de Google Cloud Console. Deze omgeving kan gebruikt worden om op basis van immutable infrastructuur principes omgevingen neer te zetten en conform de functionele specificaties beschikbaar te houden.


Iedereen heeft vooraf toegang gekregen tot Google Cloud, als je met je Google Quintgroup account in de browser sessie op je laptop zit, dan kan je direct aan de slag. 

## Leerdoelen
1. Wat houdt Provisioning in, 
2. hoe gaat dat in zijn werk in een meer Cloud Native omgeving vs een traditionele provisioning,
3. wat de rol van automatisering hierin is.

Met de onderstaande button open je de tutorial omgeving in Google Cloud. 

Bij de eerste keer dat je op de Google Cloud omgeving komt, zal dit veel pop-ups genereren, lees kort de meldingen door en vink waar nodig voor akkoord. Ook komt er een melding voorbij of je een Git repositry wil synchroniseren, klik bij deze vraag op proceed of akkoord. 

![proceed](Les 2/proceed.png)

Het kan tot 2 minuten duren, de eerste keer dat er een persoonlijke cloudshell voor je wordt aangemaakt, gedurende die tijd zie een ronddraaiend logo. Mocht e.e.a. niet het gewenste resultaat opleveren, sluit het tabblad en druk nogmaals op de knop hieronder.

**Mocht je extra beveiliging in je browser aan hebben staan tegen cookies etc. zet deze dan gedurende de tutorial uit.**

[![Open this project in Cloud
Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://github.com/quintest/cloudshell-tutorials-summerschool.git&page=editor&tutorial=provisioning/tutorial.md)

