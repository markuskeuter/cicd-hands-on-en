# Opzet en technische inrichting omgeving

Dit is een beschrijving van en handleiding voor inrichting van de training zoals deze is gegeven voor CI/CD Hands-on. De training is modulair in opbouw, onderdelen kunnen onafhankelijk van elkaar gegeven worden. Basis opzet gaat uit van gitlab.com, omdat deze omgeving al de CI runners in zich heeft, waarvan in de training in les 1 gebruik gemaakt wordt, om een statische website op gitlab-pages te publiceren en aan te passen. De kracht van de training zit m in het feit dat het een training is in CI/CD tooling over CI/CD tooling, waardoor je de omgeving niet hoeft te verlaten als cursist.

## Opzet Les 1
Alle curstisten krigen een Developer account, maar zonder merge rechten op Master, dit is voorbehouden aan de trainers. In de training doorloop je gemeenschappelijk de merge naar Master fase, waarbij in de bijbehorende slides ingegaan wordt op verschillende stijlen van inrichting met gitflow en trunkbased als de twee beschreven stijlen. De cursisten worden in 6 teams verdeeld, dit kan teruggebracht worden naar 4 teams, waarbij de oefeningen voor team 1,2,5,6 uitgevoerd worden. Deze oefeningen worden gebruikt om kennis te maken met de verschillende faseringen, waarbij cursisten een issue opvoeren op basis van de team-opdracht en de beantwoording van hoe de opdracht uit te voeren door de trainer in een beantwoording op het issue gegeven worden. De opdrachten veroorzaken een aantal conflicten, welke tijdens de merge to master opgelost moeten worden en een conflict dat niet opgemerkt wordt tijdens de merge (tekst donkerblauw en achtergrond donkerblauw).

### Technische werking Les 1
In gitlab.com zit de functionaliteit voor wat zij Auto DevOps noemen. Dit is gebaseerd op een runner implementatie waarin een configuratie bestand in de root van het project repository wordt uitgelezen en een automatisch build proces gestart wordt. Binnen gitlab zijn hiervoor zogenaamde shared runners beschikbaar gesteld. In Les 1 maken we gebruik van een standaard runner om een statische webpagina te bouwen, testen en publiceren op gitlab pages. 

## Opzet Les 2
Alle cursisten beschikken over een Google account, welke de volgende rechten hebben gekregen in de Google Cloud Omgeving die voor deze training ingezet wordt:
- Compute Admin
- Service Account User
- Viewer

De tutorial zelf is gebouwd in github.com, omdat deze omgeving wel whitelisted is voor tutorials in Google cloud shell en gitlab.com (nog) niet. Hierbij is het van belang de tekst van de tutorial aan te passen en het juiste Google cloud project te selecteren. Het huidige Google cloud project is niet meer beschikbaar voor training doeleinden, zorg dat er afstemming is over hoe deze omgeving in te richten is, bij twijfel vraag je dit na bij je practice-lead.

Overige zaken die van belang zijn: 
- richt een loadbalancer in, welke op poort 80 de achterliggende resourcepool ontsluit,
- richt de resourcepool tijdens aanmaken van loadbalancer,
- de cursisten zelf maken de machines aan en plaatsen deze in de resourcepool (zie tutorial)
- zorg dat de algemene firewall instelling http verkeer toestaan richting het defaultr netwerk (de loadbalancer heeft namelijk anders geen connectie naar de resource pool machines)
- zorg dat je NAT hebt opgezet om de machines Internet toegang te geven tijdens de initiële inrchting.

## Opzet Les 3
Dit is een korte container tutorial die ook uitgevoerd wordt in Google cloud, dit is puur uit gemak vanwege de tutorial omgeving (ook gesynchroniseerd vanuit github,com) en omdat Les 3 hierin ook heeft plaatsgevonden. Geen bijzonderheden of rechten in Google cloud noodzakelijk, de cursisten kunnen dit zelfstandig uitvoeren.