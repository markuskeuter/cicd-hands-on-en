# Les 3 Containers (hands-on) en Orchestartion (Demo)

Zoals in de les beschreven gaan we nu dieper in op Containers en Orchestration aan de hand van een tutorial waarbij we gebruik maken van containers en een orchstartion platform Google Kubernetes Engine (GKE). Ook hiervoor zullen we weer gebruik maken van de tutorial zoals deze gemaakt is voor de Google Cloud Console.

Kubernetes is een tool die oorspronkelijk bij Google ontwikkeld is, gebaseerd op de interne ervaring met een tool die Borg heette, welke gebruikt werd in de Google datacenters om de eigen cloud diensten te managen. Kubernetes is het meest populaire container orchestration platform op dit moment, het wordt door o.a. AWS, Azure, Red Hat (onder de naam OpenShift) en Google zelf als een dienst aangeboden. Je kan Kubernetes ook op eigen machines in je On-Premises datacenter draaien, op je laptop en zelfs op een setje Raspberry Pi's.


Wanneer je de omgeving opnieuw start kan het zijn dat je de volgende melding te zien krijgt, druk dan op Enter om de default waarde (1) te selecteren:

![choice](Les 3/enterYourChoice.png)

De oefening is in dezelfde omgeving gebouwd als Les 2, zodra je op de button klikt kan je hiermee aan de slag in het TAB-blad dat zich hiervoor opent.

[![Open this project in Cloud
Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://github.com/quintest/cloudshell-tutorials-summerschool.git&page=editor&tutorial=orchestration/tutorial.md)
