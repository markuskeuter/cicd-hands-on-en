# Lesson 1 Hands-on introduction Gitlab
When looking at the entire process of bringing software from the ideation phase all the way to the deployment in production phase, we refere to that process as the "pipeline". GitLab is the one of the first single suite applications for software development, security, and operations that enables DevOps, making the software lifecycle faster. GitLab provides solutions for all of the following ten steps:

![idea-to-production-10-steps](Lesson1/idea-to-production-10-steps.png)

## Idea & Code phase
Idee vorming kan je op verschillende manieren starten, bijvoorbeeld met onze Quint Design Thinking aanpak. Verder kan een Idee ontstaan tijdens een chat of email uitwisseling. De eerste stap na het identificeren van een Idee, is dat je het Idee onderbrengt in een **Issue** in Gitlab. Hiermee maak je de discussie en doorontwikkeling van het Idee inzichtelijk en traceerbaar. Hierbij maak je gebruik van de mogelijkheden om een kanban-stijl bord te gebruiken voor de Plan fase en maak je gebruik van issue tracking tools, om code of tekst aan het issue te koppelen in de Code fase. 

## Continuous Integration phase
You'll be using the power of what Gitlab calls AutoDevOps or automated CI/CD. If you want to read more on how this works, please follow this link [https://gitlab.com/help/topics/autodevops/quick_start_guide.md](https://gitlab.com/help/topics/autodevops/quick_start_guide.md), for now continue with the excersises below.

![oefening overzicht](Lesson1/20190816_CICD_Hands-on_Groep2.png)

Op basis van het Issue ga je aan de slag met de code wijziging, waarbij je een Branch (afsplitsing) van de code maakt waarin jij werkt. Afhankelijk van het soort Issue kan er daarna **Commit** (opleveren van aangepaste code), een **Merge** (terugbrengen van de code naar de Master), een geautomatiseerde **Build** en **Test** fase doorlopen worden. Ook hierbij kan gebruik gemaakt worden van de Gitlab tooling om issue te rapporteren en of een oplossing voor te stellen.

## Delivery phase
Daarna kan je de code vrijgeven voor staging en/of productie, waarbij het afhangt van de stijl die in de organisatie hanteert of er dan sprake is van Continuous Deployment (geautomatiseerde release) of meer handmatig controlle over de release en versies van de opgeleverde wijzigingen. 

Voor meer uitleg over de Gitlab Workflow kan je de volgende blog lezen: [gitlab workflow an overview](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)

# Excersise outline
Your part of the web development team of the current gitlab-pages published static website. In this team you have the role developer, which gives you certain rights to create issues, edit files within your own branche and file a merge requrest, to merge your changes to the code back to the Master branch, to be deployed again to production. As a first step you'll get to know the Gitlab interface by creating an issue with regards to the current static page: [https://markuskeuter.gitlab.io/cicd-hands-on-en](https://markuskeuter.gitlab.io/cicd-hands-on-en). If this is a cloned repository please check the URL to this repositories own gitlab-pages (settings/pages). 
Je bent nu onderdeel van het web-ontwikkelteam van quintlabs.nl en binnen GitLab heb je de rol "Developer" gekregen. GitLab heeft een rol gebasseerd toegangsmodel, met passende functionaliteiten en rechten. Als eerste ga je kennismaken met de interface, je gaat een aantal opdrachten uitvoeren om hands-on ervaring op te doen voor continuous integration. In de opeenvolgende lessen vul je deze vaardigheden aan met een kijkje onder de motorkap van de automatisering van het landschap.

## Oefening 1.1
Maak je eerste Issue aan door het volgende uit te voeren in je eigen repository:
1. Aan de linkerkant kies je in het menu Issue (Tip: rechtsklik en open in nieuw tabblad, zodat je eenvoudig naar de team uitleg terug kan..)
2. Klik op New Issue (Groene knop "New Issue")
3. Geef het Issue een titel en een korte beschrijving, elk team heeft zijn eigen issue in de Team1, Team2, etc folder hierboven staan, klik op de folder en volg de instructies op.

## Oefening 1.2
Controleer wat de status van je issue is en volg de instructies op die als antwoord op je issue staan.

## Oefening 1.3 
Pas volgens opdracht de code van de web-pagina aan, zoals aangegeven in het issue antwoord.

## Uitleg Branches en Mergers

Deployment uitleg en eindresultaat.

[Verder naar les 2](../Les 2)
