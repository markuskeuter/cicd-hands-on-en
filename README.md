[![pipeline status](https://gitlab.com/quintgroup/cicd-hands-on-nl/badges/master/pipeline.svg)](https://gitlab.com/quintgroup/cicd-hands-on-nl/commits/master)
# CI/CD & Automation Hands-on Getting Started 

This project can be used in a CI/CD or DevOps training. What does a pipeline look like in a modern CI/CD development cycle? What does the developer see, do? These are some of the questions we hope to answer in this training, by going hands-on in this training repository. The complete course will take up to 4 hours in total, but can also be broken down into the following individual lessons, where most of the hands-on excersises take up 15-20 minutes each.

- [Lesson 1 Hands-on introduction Gitlab](Lesson1)
- [Lesson 2 Provisioning](Lesson2)
- [Lesson 3 Containers and Orchestration](Lesson3)

For this training you need gitlab access and project access with developer priveledges (not maintainer, only the trainer gets to be the maintainer). Further Google Cloud access is required and a project needs to be setup, where students can create the virtual machines in lesson 2. 

## Trainer guidens
When you would like to setup (parts) of this training and need more input, please contact me directly.
